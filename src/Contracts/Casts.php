<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts;

use Smorken\Athena\Constants\ColumnType;

interface Casts
{
    public function cast(string $value, ColumnType $type): mixed;
}
