<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Actions;

use Smorken\Athena\Actions\QueryData\QueryExecutionId;
use Smorken\Athena\Actions\StartQueryExecution\Data\StartQueryExecutionParams as Params;

interface StartQueryExecutionAction extends Action, WithClient
{
    public function __invoke(Params $params): QueryExecutionId;
}
