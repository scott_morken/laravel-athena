<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Actions;

use Aws\ResultInterface;

interface HasResult
{
    public static function fromArray(array $raw): static;

    public static function fromAwsResult(ResultInterface $result): static;
}
