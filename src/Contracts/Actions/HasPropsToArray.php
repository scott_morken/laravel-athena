<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Actions;

use Illuminate\Contracts\Support\Arrayable;

interface HasPropsToArray extends Arrayable {}
