<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Actions;

interface Action {}
