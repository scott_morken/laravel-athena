<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Actions;

use Smorken\Athena\Actions\GetQueryExecution\Data\GetQueryExecutionResult;
use Smorken\Athena\Actions\QueryData\QueryExecutionId;

interface GetQueryExecutionAction extends Action, WithClient
{
    public function __invoke(QueryExecutionId $queryExecutionId): GetQueryExecutionResult;
}
