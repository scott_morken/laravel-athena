<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Actions;

use Aws\Athena\AthenaClient;

interface WithClient
{
    public function getClient(): AthenaClient;
}
