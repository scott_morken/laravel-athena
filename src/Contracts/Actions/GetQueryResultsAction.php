<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Actions;

use Smorken\Athena\Actions\GetQueryResults\Data\GetQueryResults;
use Smorken\Athena\Actions\GetQueryResults\Data\GetQueryResultsParams;

interface GetQueryResultsAction extends Action, WithClient
{
    public function __invoke(GetQueryResultsParams $params): GetQueryResults;
}
