<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Parts;

use Aws\ResultInterface;
use Smorken\Athena\Contracts\Actions\HasPropsToArray;

interface Result extends HasPropsToArray
{
    public static function fromArray(array $raw): static;

    public static function fromAwsResult(ResultInterface $result): static;
}
