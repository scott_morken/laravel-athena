<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Parts;

use Smorken\Athena\Contracts\Actions\HasPropsToArray;

interface Param extends HasPropsToArray {}
