<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Parts;

use Smorken\Athena\Constants\ColumnType;

interface Cast
{
    public function cast(string $value): mixed;

    public function handles(ColumnType $type): bool;
}
