<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Generators;

use Illuminate\Contracts\Support\Arrayable;
use Smorken\Athena\Actions\GetQueryResults\Data\Datum;
use Smorken\Athena\Actions\GetQueryResults\Data\ResultSetMetadata;
use Smorken\Athena\Actions\GetQueryResults\Data\Row;

interface Attributes extends Arrayable
{
    public function add(Attribute $attribute): self;

    public function fromDatum(Datum $datum, int $columnIndex): self;

    public function isRowMetadata(Row $row): bool;

    public function metadata(): ResultSetMetadata;

    public function newFromRow(Row $row): self;
}
