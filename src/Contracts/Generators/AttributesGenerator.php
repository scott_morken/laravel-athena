<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Generators;

use Smorken\Athena\Actions\GetQueryResults\Data\Rows;

interface AttributesGenerator
{
    /**
     * @return \Generator<\Smorken\Athena\Contracts\Generators\Attributes>
     */
    public function __invoke(Rows $rows): \Generator;

    public function toArray(Rows $rows): array;
}
