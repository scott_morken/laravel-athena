<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Generators;

use Illuminate\Contracts\Support\Arrayable;

/**
 * @property string $name
 * @property string $raw
 * @property \Smorken\Athena\Constants\ColumnType $type
 *
 * @phpstan-require-extends \Smorken\Athena\Generators\Attribute
 */
interface Attribute extends Arrayable
{
    public function value(): mixed;

    public static function setCastsResolver(\Closure $closure): void;
}
