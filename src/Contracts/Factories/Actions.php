<?php

declare(strict_types=1);

namespace Smorken\Athena\Contracts\Factories;

use Aws\Athena\AthenaClient;
use Smorken\Athena\Contracts\Actions\Action;

interface Actions
{
    public function get(string $contract, AthenaClient $client): Action;
}
