<?php

declare(strict_types=1);

namespace Smorken\Athena\Constants;

enum ColumnType: string
{
    case VARCHAR = 'varchar';

    case DATE = 'date';

    case URI = 'uri';

    case INTEGER = 'integer';

    case BOOLEAN = 'boolean';

    case TINY_INT = 'tinyint';

    case SMALL_INT = 'smallint';

    case INT = 'int';

    case BIG_INT = 'bigint';

    case DOUBLE = 'double';

    case FLOAT = 'float';

    case DECIMAL = 'decimal';

    case CHAR = 'char';

    case STRING = 'string';

    case BINARY = 'binary';

    case TIMESTAMP = 'timestamp';
}
