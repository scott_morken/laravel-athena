<?php

declare(strict_types=1);

namespace Smorken\Athena\Constants;

enum S3AclOption: string
{
    case BUCKET_OWNER_FULL_CONTROL = 'BUCKET_OWNER_FULL_CONTROL';
}
