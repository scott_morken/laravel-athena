<?php

declare(strict_types=1);

namespace Smorken\Athena\Constants;

enum QueryExecutionState: string
{
    case QUEUED = 'QUEUED';
    case RUNNING = 'RUNNING';
    case SUCCEEDED = 'SUCCEEDED';
    case CANCELLED = 'CANCELLED';
    case FAILED = 'FAILED';

    case EXCEPTION = 'EXCEPTION';

    public function isCompleted(): bool
    {
        return $this === self::SUCCEEDED;
    }

    public function isFailed(): bool
    {
        return $this === self::FAILED || $this === self::CANCELLED;
    }

    public function isPending(): bool
    {
        return $this === self::QUEUED || $this === self::RUNNING;
    }
}
