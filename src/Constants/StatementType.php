<?php

declare(strict_types=1);

namespace Smorken\Athena\Constants;

enum StatementType: string
{
    case DDL = 'DDL';
    case DML = 'DML';
    case UTILITY = 'UTILITY';
}
