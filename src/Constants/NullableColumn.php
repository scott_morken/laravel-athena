<?php

declare(strict_types=1);

namespace Smorken\Athena\Constants;

enum NullableColumn: string
{
    case UNKNOWN = 'UNKNOWN';

    case NOT_NULL = 'NOT_NULL';

    case NULLABLE = 'NULLABLE';

    public function isNullable(): bool
    {
        return $this !== self::NOT_NULL;
    }
}
