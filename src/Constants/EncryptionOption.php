<?php

declare(strict_types=1);

namespace Smorken\Athena\Constants;

enum EncryptionOption: string
{
    case SSE_S3 = 'SSE_S3';
    case SSE_KMS = 'SSE_KMS';

    case CSE_KMS = 'CSE_KMS';
}
