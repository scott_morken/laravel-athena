<?php

declare(strict_types=1);

namespace Smorken\Athena\Database\Schema;

use Illuminate\Database\Schema\MySqlBuilder;

class Builder extends MySqlBuilder {}
