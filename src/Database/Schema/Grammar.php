<?php

declare(strict_types=1);

namespace Smorken\Athena\Database\Schema;

use Illuminate\Database\Schema\Grammars\MySqlGrammar;

class Grammar extends MySqlGrammar {}
