<?php

declare(strict_types=1);

namespace Smorken\Athena\Database;

use Aws\Athena\AthenaClient;
use Illuminate\Database\Connection;
use Illuminate\Database\QueryException;
use Illuminate\Database\UniqueConstraintViolationException;
use Smorken\Athena\Actions\AthenaClient\Data\AthenaClientParams;
use Smorken\Athena\Actions\AthenaClient\Data\Credentials;
use Smorken\Athena\Database\Query\Grammar;
use Smorken\Athena\Database\Query\Processor;
use Smorken\Athena\Database\Schema\Builder;

class AthenaConnection extends Connection
{
    public static ActionExecutor $actionExecutor;

    /**
     * @var array<string, AthenaClient>
     */
    protected static array $clients = [];

    public function __construct(array $config)
    {
        $this->config = $config;
        $this->database = $config['database'];
        $this->tablePrefix = $config['table_prefix'] ?? '';
        $this->initAthenaClient($config);

        // We need to initialize a query grammar and the query post processors
        // which are both very important parts of the database abstractions
        // so we initialize these to their default values while starting.
        $this->useDefaultQueryGrammar();

        $this->useDefaultPostProcessor();
    }

    public static function addAthenaClient(AthenaClient $athenaClient, string $name): void
    {
        self::$clients[$name] = $athenaClient;
    }

    /**
     * Run an SQL statement and get the number of rows affected.
     *
     * @param  string  $query
     * @param  array  $bindings
     * @return int
     */
    public function affectingStatement($query, $bindings = [])
    {
        return $this->run($query, $bindings, function ($query, $bindings) {
            if ($this->pretending()) {
                return 0;
            }

            $prepared = $this->prepareBindings($bindings);
            $affected = $this->getActionExecutor()->affecting($query, $prepared);

            $this->recordsHaveBeenModified(
                $affected > 0
            );

            return $affected;
        });
    }

    public function cursor($query, $bindings = [], $useReadPdo = true)
    {
        return $this->run($query, $bindings, function ($query, $bindings) {
            if ($this->pretending()) {
                return [];
            }

            $prepared = $this->prepareBindings($bindings);

            return $this->getActionExecutor()->cursor($query, $prepared);
        });
    }

    public function getSchemaBuilder(): Builder
    {
        // @phpstan-ignore function.impossibleType
        if (is_null($this->schemaGrammar)) {
            $this->useDefaultSchemaGrammar();
        }

        return new Builder($this);
    }

    public function select($query, $bindings = [], $useReadPdo = true)
    {
        return $this->run($query, $bindings, function ($query, $bindings) {
            if ($this->pretending()) {
                return [];
            }
            $prepared = $this->prepareBindings($bindings);

            return $this->getActionExecutor()->select($query, $prepared);
        });
    }

    public function selectResultSets($query, $bindings = [], $useReadPdo = true)
    {
        return $this->run($query, $bindings, function ($query, $bindings) {
            if ($this->pretending()) {
                return [];
            }

            $prepared = $this->prepareBindings($bindings);

            return $this->getActionExecutor()->selectResultSets($query, $prepared);
        });
    }

    public function statement($query, $bindings = [])
    {
        return $this->run($query, $bindings, function ($query, $bindings) {
            if ($this->pretending()) {
                return true;
            }
            $prepared = $this->prepareBindings($bindings);

            $this->recordsHaveBeenModified();

            return $this->getActionExecutor()->query($query, $prepared);
        });
    }

    /**
     * Run a raw, unprepared query against the PDO connection.
     *
     * @param  string  $query
     * @return bool
     */
    public function unprepared($query)
    {
        return $this->run($query, [], function ($query) {
            if ($this->pretending()) {
                return true;
            }

            throw new \Exception('Unprepared queries are not supported');
        });
    }

    protected function getActionExecutor(): ActionExecutor
    {
        return self::$actionExecutor;
    }

    protected function getAthenaClient(array $config): AthenaClient
    {
        $name = $config['name'] ?? 'athena';

        return self::$clients[$name] ?? new AthenaClient($this->getAthenaClientParams($config)->toArray());
    }

    protected function getAthenaClientParams(array $config): AthenaClientParams
    {
        return new AthenaClientParams(
            new Credentials($config['credentials']['key'], $config['credentials']['secret']),
            $config['region'],
            $config['version'],
            $config['debug'] ?? false
        );
    }

    protected function getDefaultPostProcessor(): Processor
    {
        return new Processor();
    }

    protected function getDefaultQueryGrammar(): \Illuminate\Database\Query\Grammars\Grammar
    {
        ($grammar = new Grammar)->setConnection($this);

        // @phpstan-ignore return.type
        return $this->withTablePrefix($grammar);
    }

    protected function getDefaultSchemaGrammar(): ?\Illuminate\Database\Schema\Grammars\Grammar
    {
        ($grammar = new \Smorken\Athena\Database\Schema\Grammar())->setConnection($this);

        // @phpstan-ignore return.type
        return $this->withTablePrefix($grammar);
    }

    protected function initAthenaClient(array $config): void
    {
        $client = $this->getAthenaClient($config);
        $this->getActionExecutor()->init($client, $config);
    }

    protected function runQueryCallback($query, $bindings, \Closure $callback)
    {
        // To execute the statement, we'll simply call the callback, which will actually
        // run the SQL against the PDO connection. Then we can calculate the time it
        // took to execute and log the query SQL, bindings and time in our memory.
        try {
            return $callback($query, $bindings);
        } catch (\Aws\Athena\Exception\AthenaException $e) {
            throw $e;
        }

        // If an exception occurs when attempting to run a query, we'll format the error
        // message to include the bindings with SQL, which will make this exception a
        // lot more helpful to the developer instead of just the database's errors.
        catch (\Exception $e) {
            if ($this->isUniqueConstraintError($e)) {
                throw new UniqueConstraintViolationException(
                    $this->getName(), $query, $this->prepareBindings($bindings), $e
                );
            }

            throw new QueryException(
                $this->getName(), $query, $this->prepareBindings($bindings), $e
            );
        }
    }
}
