<?php

declare(strict_types=1);

namespace Smorken\Athena\Database;

use Smorken\Athena\Actions\QueryData\AthenaError;
use Smorken\Athena\Constants\QueryExecutionState;

class AthenaQueryException extends \Exception
{
    public function __construct(
        QueryExecutionState $state,
        string $message = '',
        int $code = 0,
        ?\Throwable $previous = null
    ) {
        $message = sprintf('Athena Query [%s]: %s', $state->value, $message);
        parent::__construct($message, $code, $previous);
    }

    public static function athenaError(QueryExecutionState $state, AthenaError $error): self
    {
        return new self($state, $error->errorMessage);
    }

    public static function queryException(QueryExecutionState $state, string $message): self
    {
        return new self($state, $message);
    }
}
