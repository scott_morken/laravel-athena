<?php

declare(strict_types=1);

namespace Smorken\Athena\Database;

class AthenaException extends \Exception
{
    public function __construct(
        public string $called,
        public string $response,
        public string $errorCode,
        public array $detail,
        string $message = '',
        int $code = 0,
        ?\Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
