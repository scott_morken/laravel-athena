<?php

declare(strict_types=1);

namespace Smorken\Athena\Database\Query;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Query\Expression as ExpressionContract;

class Builder extends \Smorken\Model\QueryBuilders\Builder
{
    public function whereTimestamp($column, $operator, $value = null, $boolean = 'and'): Builder
    {
        $type = 'Timestamp';
        [$value, $operator] = $this->query->prepareValueAndOperator(
            $value, $operator, func_num_args() === 2
        );
        $value = $this->ensureAthenaTimestamp($value);
        $this->query->wheres[] = compact('column', 'type', 'boolean', 'operator', 'value');
        if (! $value instanceof ExpressionContract) {
            $this->addBinding($value, 'where');
        }

        return $this;
    }

    protected function ensureAthenaTimestamp($value)
    {
        if ($value instanceof ExpressionContract) {
            return $value;
        }

        return $this->formatForAthenaTimestamp($value);
    }

    protected function formatForAthenaTimestamp(string|Carbon $date): string
    {
        if (is_a($date, Carbon::class)) {
            return $date->toIso8601String();
        }
        try {
            return Carbon::parse($date)->toIso8601String();
        } catch (\Throwable) {
            return $date;
        }
    }
}
