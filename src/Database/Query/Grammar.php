<?php

declare(strict_types=1);

namespace Smorken\Athena\Database\Query;

use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\MySqlGrammar;

class Grammar extends MySqlGrammar
{
    protected $selectComponents = [
        'aggregate',
        'columns',
        'from',
        'indexHint',
        'joins',
        'wheres',
        'groups',
        'havings',
        'orders',
        'offset',
        'limit',
        'lock',
    ];

    protected function compileLimit(\Illuminate\Database\Query\Builder $query, $limit)
    {
        $limit = (int) $limit;

        if ($limit && $query->offset > 0) {
            return "fetch next {$limit} rows only";
        }

        return parent::compileLimit($query, $limit);
    }

    /**
     * Compile the "offset" portions of the query.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @param  int  $offset
     * @return string
     */
    protected function compileOffset(Builder $query, $offset)
    {
        $offset = (int) $offset;

        if ($offset) {
            return "offset {$offset} rows";
        }

        return '';
    }

    protected function whereTimestamp(Builder $query, $where)
    {
        $value = $this->parameter($where['value']);
        $operator = $where['operator'] ?? '=';

        return $this->wrap($where['column']).' '.$operator.' from_iso8601_timestamp('.$value.')';
    }

    protected function wrapValue($value)
    {
        return $value === '*' ? $value : '"'.str_replace("'", "''", $value).'"';
    }
}
