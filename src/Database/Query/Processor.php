<?php

declare(strict_types=1);

namespace Smorken\Athena\Database\Query;

use Illuminate\Database\Query\Processors\MySqlProcessor;

class Processor extends MySqlProcessor {}
