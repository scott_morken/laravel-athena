<?php

declare(strict_types=1);

namespace Smorken\Athena\Database;

use Aws\Athena\AthenaClient;
use Smorken\Athena\Actions\GetQueryExecution\Data\GetQueryExecutionResult;
use Smorken\Athena\Actions\GetQueryResults\Data\GetQueryResults;
use Smorken\Athena\Actions\GetQueryResults\Data\Rows;
use Smorken\Athena\Actions\GetQueryResults\ParamsBuilder as ParamsBuilderForGetQueryResults;
use Smorken\Athena\Actions\QueryData\QueryExecutionId;
use Smorken\Athena\Actions\StartQueryExecution\ParamsBuilder;
use Smorken\Athena\Contracts\Actions\Action;
use Smorken\Athena\Contracts\Actions\GetQueryExecutionAction;
use Smorken\Athena\Contracts\Actions\GetQueryResultsAction;
use Smorken\Athena\Contracts\Actions\StartQueryExecutionAction;
use Smorken\Athena\Contracts\Factories\Actions;
use Smorken\Athena\Generators\AttributesGenerator;
use Smorken\Athena\Support\RateLimit;

class ActionExecutor
{
    protected AthenaClient $client;

    protected array $config;

    protected bool $initialized = false;

    protected int $retryCount = 0;

    public function __construct(protected Actions $actionsFactory, protected RateLimit $rateLimit) {}

    public function affecting(string $query, array $bindings = []): int
    {
        $executionId = $this->newQuery($query, $bindings);
        $results = $this->getQueryResults($executionId);

        return $results->updateCount;
    }

    public function cursor(string $query, array $bindings = []): \Generator
    {
        $executionId = $this->newQuery($query, $bindings);

        return $this->yieldResults($executionId);
    }

    public function getClient(): AthenaClient
    {
        return $this->client;
    }

    public function init(AthenaClient $client, array $config): void
    {
        $this->config = $config;
        $this->client = $client;
        $this->initialized = true;
    }

    public function isInitialized(): bool
    {
        return $this->initialized;
    }

    public function query(string $query, array $bindings = []): array
    {
        $executionId = $this->newQuery($query, $bindings);

        return $this->getResults($executionId);
    }

    public function select(string $query, array $bindings): array
    {
        return $this->query($query, $bindings);
    }

    public function selectResultSets(string $query, array $bindings): array
    {
        $executionId = $this->newQuery($query, $bindings);

        return $this->getResultsAsResultSet($executionId);
    }

    protected function getAction(string $contract): Action
    {
        return $this->actionsFactory->get($contract, $this->client);
    }

    protected function getQueryExecution(QueryExecutionId $queryExecutionId): GetQueryExecutionResult
    {
        /** @var GetQueryExecutionAction $action */
        $action = $this->getAction(GetQueryExecutionAction::class);

        return $action($queryExecutionId);
    }

    protected function getQueryResults(
        QueryExecutionId $queryExecutionId,
        ?string $nextToken = null,
        ?int $maxResults = null
    ): GetQueryResults {
        $params = (new ParamsBuilderForGetQueryResults($this->config))->params(
            $queryExecutionId,
            $nextToken,
            $maxResults
        );

        /** @var GetQueryResultsAction $action */
        $action = $this->getAction(GetQueryResultsAction::class);

        return $action($params);
    }

    protected function getResults(QueryExecutionId $queryExecutionId): array
    {
        $results = [];
        $r = $this->getQueryResults($queryExecutionId);
        $attributesGenerator = new AttributesGenerator($r->resultSet->resultSetMetadata);
        $this->queryResultsToArray($attributesGenerator, $r->resultSet->rows, $results);
        while ($r->nextToken) {
            $r = $this->getQueryResults($queryExecutionId, $r->nextToken);
            $this->queryResultsToArray($attributesGenerator, $r->resultSet->rows, $results);
        }

        return $results;
    }

    protected function getResultsAsResultSet(QueryExecutionId $queryExecutionId): array
    {
        $results = [];
        $r = $this->getQueryResults($queryExecutionId);
        $attributesGenerator = new AttributesGenerator($r->resultSet->resultSetMetadata);
        $results[] = $this->queryResultsAsArray($attributesGenerator, $r->resultSet->rows);
        while ($r->nextToken) {
            $r = $this->getQueryResults($queryExecutionId, $r->nextToken);
            $results[] = $this->queryResultsAsArray($attributesGenerator, $r->resultSet->rows);
        }

        return $results;
    }

    protected function newQuery(string $query, array $bindings): QueryExecutionId
    {
        $executionId = $this->startQuery($query, $bindings);
        $queryExecution = $this->waitForQuery($executionId);
        $this->throwErrorsForGetQueryExecution($queryExecution);

        return $executionId;
    }

    protected function queryResultsAsArray(AttributesGenerator $attributesGenerator, Rows $rows): array
    {
        $results = [];
        foreach ($attributesGenerator($rows) as $item) {
            $results[] = $item->toArray();
        }

        return $results;
    }

    protected function queryResultsToArray(AttributesGenerator $attributesGenerator, Rows $rows, array &$results): void
    {
        foreach ($attributesGenerator($rows) as $item) {
            $results[] = $item->toArray();
        }
    }

    protected function startQuery(string $query, array $bindings): QueryExecutionId
    {
        $params = (new ParamsBuilder($this->config))->params($query, $bindings);
        /** @var StartQueryExecutionAction $action */
        $action = $this->getAction(StartQueryExecutionAction::class);

        return $action($params);
    }

    /**
     * @throws \Smorken\Athena\Database\AthenaQueryException
     */
    protected function throwErrorsForGetQueryExecution(GetQueryExecutionResult $queryExecution): void
    {
        if ($queryExecution->isFailed()) {
            $state = $queryExecution->queryExecution->status->state;
            $athenaError = $queryExecution->queryExecution->status->athenaError;
            if ($athenaError) {
                throw AthenaQueryException::athenaError($state, $athenaError);
            }
            $stateChangeReason = $queryExecution->queryExecution->status->stateChangeReason;
            if ($stateChangeReason && stripos($stateChangeReason, 'Partition already exists') === false) {
                throw AthenaQueryException::queryException($state, $stateChangeReason);
            }
        }
    }

    protected function waitForQuery(QueryExecutionId $queryExecutionId): GetQueryExecutionResult
    {
        $result = $this->getQueryExecution($queryExecutionId);
        while ($result->isPending()) {
            $this->rateLimit->sleep();
            $result = $this->getQueryExecution($queryExecutionId);
        }
        $this->rateLimit->reset();

        return $result;
    }

    protected function yieldResults(QueryExecutionId $queryExecutionId): \Generator
    {
        $r = $this->getQueryResults($queryExecutionId);
        $attributesGenerator = new AttributesGenerator($r->resultSet->resultSetMetadata);
        foreach ($attributesGenerator($r->resultSet->rows) as $row) {
            yield $row->toArray();
        }
        while ($r->nextToken) {
            $r = $this->getQueryResults($queryExecutionId, $r->nextToken);
            foreach ($attributesGenerator($r->resultSet->rows) as $row) {
                yield $row->toArray();
            }
        }
    }
}
