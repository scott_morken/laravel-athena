<?php

declare(strict_types=1);

namespace Smorken\Athena\Database;

use Smorken\Athena\Database\Query\Builder;

class Eloquent extends \Smorken\Model\Eloquent
{
    protected static string $builder = Builder::class;

    protected $connection = 'athena';
}
