<?php

declare(strict_types=1);

namespace Smorken\Athena\Concerns;

use Smorken\Athena\Contracts\Actions\HasPropsToArray;

trait HasPropertiesToArray
{
    protected array $propertyMap = [];

    protected bool $ucFirstChar = true;

    public function toArray(): array
    {
        $mapped = [];
        foreach ($this->getPropertyMap() as $property => $mappedName) {
            $p = $this->$property;
            if (is_a($p, HasPropsToArray::class)) {
                $p = $p->toArray();
            }
            if (! $this->isEmpty($p)) {
                $mapped[$mappedName] = $p;
            }
        }

        return $mapped;
    }

    protected function getPropertyMap(): array
    {
        if (! $this->propertyMap) {
            $props = $this->reflectSelf()->getProperties(\ReflectionProperty::IS_PUBLIC);
            foreach ($props as $prop) {
                $this->propertyMap[$prop->getName()] = $this->getVirtualPropertyName($prop->getName());
            }
        }

        return $this->propertyMap;
    }

    protected function getVirtualPropertyName(string $property): string
    {
        if ($this->ucFirstChar) {
            return ucfirst($property);
        }

        return $property;
    }

    protected function isEmpty(mixed $value): bool
    {
        return empty($value) && ! is_numeric($value) && ! is_bool($value);
    }

    protected function reflectSelf(): \ReflectionClass
    {
        return new \ReflectionClass($this);
    }
}
