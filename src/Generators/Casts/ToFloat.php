<?php

declare(strict_types=1);

namespace Smorken\Athena\Generators\Casts;

use Smorken\Athena\Constants\ColumnType;

class ToFloat extends BaseCast
{
    protected array $types = [ColumnType::DOUBLE, ColumnType::FLOAT, ColumnType::DECIMAL];

    public function cast(string $value): float
    {
        return (float) $value;
    }
}
