<?php

declare(strict_types=1);

namespace Smorken\Athena\Generators\Casts;

use Carbon\Carbon;
use Smorken\Athena\Constants\ColumnType;

class ToCarbon extends BaseCast
{
    protected array $types = [ColumnType::DATE, ColumnType::TIMESTAMP];

    public function cast(string $value): Carbon
    {
        return Carbon::parse($value);
    }
}
