<?php

declare(strict_types=1);

namespace Smorken\Athena\Generators\Casts;

use Smorken\Athena\Constants\ColumnType;

class ToInteger extends BaseCast
{
    protected array $types = [
        ColumnType::INTEGER, ColumnType::INT, ColumnType::BIG_INT, ColumnType::TINY_INT, ColumnType::SMALL_INT,
    ];

    public function cast(string $value): int
    {
        return (int) $value;
    }
}
