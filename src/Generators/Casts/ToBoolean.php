<?php

declare(strict_types=1);

namespace Smorken\Athena\Generators\Casts;

use Smorken\Athena\Constants\ColumnType;

class ToBoolean extends BaseCast
{
    protected array $types = [ColumnType::BOOLEAN];

    public function cast(string $value): bool
    {
        $lower = strtolower($value);
        if ($lower === 'false') {
            return false;
        }
        if ($lower === 'true') {
            return true;
        }

        return (bool) $value;
    }
}
