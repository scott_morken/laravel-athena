<?php

declare(strict_types=1);

namespace Smorken\Athena\Generators\Casts;

use Smorken\Athena\Constants\ColumnType;
use Smorken\Athena\Contracts\Parts\Cast;

abstract class BaseCast implements Cast
{
    protected array $types = [];

    public function handles(ColumnType $type): bool
    {
        return in_array($type, $this->types, true);
    }
}
