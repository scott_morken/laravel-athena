<?php

declare(strict_types=1);

namespace Smorken\Athena\Generators;

use Smorken\Athena\Constants\ColumnType;
use Smorken\Athena\Constants\NullableColumn;
use Smorken\Athena\Contracts\Casts;

class Attribute implements \Smorken\Athena\Contracts\Generators\Attribute
{
    protected static \Closure $castsResolver;

    protected Casts $casts;

    protected bool $eval = false;

    protected mixed $value = null;

    public function __construct(
        public string $name,
        public string $raw,
        public ColumnType $type,
        public NullableColumn $nullable
    ) {
        $this->casts = $this->resolveCasts();
    }

    public static function setCastsResolver(\Closure $closure): void
    {
        self::$castsResolver = $closure;
    }

    public function toArray(): array
    {
        return [$this->name => $this->value()];
    }

    public function value(): mixed
    {
        if ($this->eval === false) {
            if (! $this->wantsNull($this->raw)) {
                $this->value = $this->casts->cast($this->raw, $this->type);
            }
            $this->eval = true;
        }

        return $this->value;
    }

    protected function isEmpty(mixed $value): bool
    {
        return empty($value) && ! is_numeric($value) && ! is_bool($value);
    }

    protected function resolveCasts(): Casts
    {
        return (self::$castsResolver)();
    }

    protected function wantsNull(mixed $value): bool
    {
        return $this->isEmpty($value) && $this->nullable->isNullable();
    }
}
