<?php

declare(strict_types=1);

namespace Smorken\Athena\Generators;

use Smorken\Athena\Actions\GetQueryResults\Data\ColumnInfo;
use Smorken\Athena\Actions\GetQueryResults\Data\Datum;
use Smorken\Athena\Actions\GetQueryResults\Data\ResultSetMetadata;
use Smorken\Athena\Actions\GetQueryResults\Data\Row;
use Smorken\Athena\Contracts\Generators\Attribute;

class Attributes implements \Smorken\Athena\Contracts\Generators\Attributes
{
    /**
     * @var array<string, mixed>
     */
    protected array $cached = [];

    /**
     * @var array<int, \Smorken\Athena\Contracts\Generators\Attribute>
     */
    protected array $attributes = [];

    public function __construct(protected ResultSetMetadata $metadata) {}

    public function add(Attribute $attribute): \Smorken\Athena\Contracts\Generators\Attributes
    {
        $this->attributes[] = $attribute;

        return $this;
    }

    public function fromDatum(Datum $datum, int $columnIndex): \Smorken\Athena\Contracts\Generators\Attributes
    {
        $columnInfo = $this->getColumnInfo($columnIndex);
        $this->add($this->attributeFromDatumAndColumnInfo($datum, $columnInfo));

        return $this;
    }

    public function isRowMetadata(Row $row): bool
    {
        /**
         * @var int $index
         * @var \Smorken\Athena\Actions\GetQueryResults\Data\Datum $datum
         */
        foreach ($row as $index => $datum) {
            if (! $this->isDatumColumnName($index, $datum)) {
                return false;
            }

            return true;
        }

        return false;
    }

    public function metadata(): ResultSetMetadata
    {
        return $this->metadata;
    }

    public function newFromRow(Row $row): \Smorken\Athena\Contracts\Generators\Attributes
    {
        $instance = new self($this->metadata());
        if ($this->isRowMetadata($row)) {
            return $instance;
        }
        /**
         * @var int $index
         * @var Datum $datum
         */
        foreach ($row as $index => $datum) {
            $instance->fromDatum($datum, $index);
        }

        return $instance;
    }

    public function toArray(): array
    {
        if (! $this->cached) {
            foreach ($this->attributes as $attribute) {
                $this->cached[$attribute->name] = $attribute->value();
            }
        }

        return $this->cached;
    }

    protected function attributeFromDatumAndColumnInfo(Datum $datum, ColumnInfo $columnInfo): Attribute
    {
        return new \Smorken\Athena\Generators\Attribute(
            $columnInfo->name,
            $datum->varCharValue,
            $columnInfo->type,
            $columnInfo->nullable
        );
    }

    protected function getColumnInfo(int $index): ColumnInfo
    {
        return $this->metadata()[$index];
    }

    protected function isDatumColumnName(int $index, Datum $datum): bool
    {
        $columnInfo = $this->getColumnInfo($index);

        return $datum->varCharValue === $columnInfo->name;
    }
}
