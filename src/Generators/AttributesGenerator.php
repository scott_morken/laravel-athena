<?php

declare(strict_types=1);

namespace Smorken\Athena\Generators;

use Smorken\Athena\Actions\GetQueryResults\Data\ResultSetMetadata;
use Smorken\Athena\Actions\GetQueryResults\Data\Rows;

class AttributesGenerator implements \Smorken\Athena\Contracts\Generators\AttributesGenerator
{
    public function __construct(
        protected ResultSetMetadata $resultSetMetadata
    ) {}

    public function __invoke(Rows $rows): \Generator
    {
        $attributes = new Attributes($this->resultSetMetadata);
        /** @var \Smorken\Athena\Actions\GetQueryResults\Data\Row $row */
        foreach ($rows as $row) {
            if (! $attributes->isRowMetadata($row)) {
                yield $attributes->newFromRow($row);
            }
        }
    }

    public function toArray(Rows $rows): array
    {
        $arrayed = [];
        foreach ($this($rows) as $attributes) {
            $arrayed[] = $attributes->toArray();
        }

        return $arrayed;
    }
}
