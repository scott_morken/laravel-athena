<?php

declare(strict_types=1);

namespace Smorken\Athena;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Foundation\CachesConfiguration;
use Smorken\Athena\Contracts\Casts;
use Smorken\Athena\Contracts\Factories\Actions;
use Smorken\Athena\Database\ActionExecutor;
use Smorken\Athena\Database\AthenaConnection;
use Smorken\Athena\Generators\Attribute;
use Smorken\Athena\Generators\Casts\ToBoolean;
use Smorken\Athena\Generators\Casts\ToCarbon;
use Smorken\Athena\Generators\Casts\ToFloat;
use Smorken\Athena\Generators\Casts\ToInteger;
use Smorken\Athena\Support\CastFromStringToColumnType;
use Smorken\Athena\Support\RateLimit;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfig();
        Attribute::setCastsResolver(fn () => $this->app[Casts::class]);
        AthenaConnection::$actionExecutor = new ActionExecutor($this->app[Actions::class], new RateLimit());
    }

    public function register(): void
    {
        $this->registerCasts();
        $this->registerActions();
        $this->registerAthenaDatabaseDriver();
    }

    protected function addDatabaseConnection(): void
    {
        if (file_exists(config_path('sm-athena.php'))) {
            $config = require config_path('sm-athena.php');
        } else {
            $config = require __DIR__.'/../config/athena.php';
        }
        $this->mergeConfig($config['database'] ?? [], 'database.connections.athena');
    }

    protected function bootConfig(): void
    {
        $this->publishes([
            __DIR__.'/../config/athena.php' => config_path('sm-athena.php'),
        ], 'sm-athena');
        $this->addDatabaseConnection();
        $this->mergeConfigFrom(__DIR__.'/../config/athena.php', 'sm-athena');
    }

    protected function mergeConfig(array $merge, string $key): void
    {
        if (! ($this->app instanceof CachesConfiguration && $this->app->configurationIsCached())) {
            $config = $this->app->make('config');

            $config->set($key, array_merge(
                $merge, $config->get($key, [])
            ));
        }
    }

    protected function registerActions(): void
    {
        $this->app->scoped(Actions::class, function (Application $app) {
            $actions = $app['config']['sm-athena']['actions'] ?? [];

            return new \Smorken\Athena\Factories\Actions($actions);
        });
    }

    protected function registerAthenaDatabaseDriver(): void
    {
        $this->app->resolving('db', function ($db) {
            $db->extend('athena', function ($config, $name) {
                $config['name'] = $name;

                return new AthenaConnection($config);
            });
        });
    }

    protected function registerCasts(): void
    {
        $this->app->scoped(Casts::class, function () {
            return new CastFromStringToColumnType([
                new ToBoolean(),
                new ToCarbon(),
                new ToFloat(),
                new ToInteger(),
            ]);
        });
    }
}
