<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\StartQueryExecution;

use Smorken\Athena\Actions\BaseAction;
use Smorken\Athena\Actions\QueryData\QueryExecutionId;
use Smorken\Athena\Actions\StartQueryExecution\Data\StartQueryExecutionParams as Params;

class StartQueryExecutionAction extends BaseAction implements \Smorken\Athena\Contracts\Actions\StartQueryExecutionAction
{
    public function __invoke(Params $params): QueryExecutionId
    {
        $result = $this->getClient()->startQueryExecution($params->toArray());

        return QueryExecutionId::fromAwsResult($result);
    }
}
