<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\StartQueryExecution\Data;

use Smorken\Athena\Actions\Data\BaseData;
use Smorken\Athena\Actions\QueryData\QueryExecutionContext;
use Smorken\Athena\Actions\QueryData\ResultConfiguration;
use Smorken\Athena\Actions\QueryData\ResultReuseConfiguration;

class StartQueryExecutionParams extends BaseData
{
    /**
     * @param  array<int, string>|null  $executionParameters
     */
    public function __construct(
        public string $queryString,
        public ?array $executionParameters = null,
        public ?QueryExecutionContext $queryExecutionContext = null,
        public ?ResultConfiguration $resultConfiguration = null,
        public ?ResultReuseConfiguration $resultReuseConfiguration = null,
        public ?string $workGroup = null,
        public ?string $clientRequestToken = null,
    ) {}
}
