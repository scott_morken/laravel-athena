<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\StartQueryExecution;

use Smorken\Athena\Actions\QueryData\QueryExecutionContext;
use Smorken\Athena\Actions\QueryData\ResultConfiguration;
use Smorken\Athena\Actions\StartQueryExecution\Data\StartQueryExecutionParams;

class ParamsBuilder
{
    public function __construct(protected array $config) {}

    public function params(string $query, array $bindings): StartQueryExecutionParams
    {
        return new StartQueryExecutionParams(
            $query,
            $bindings,
            $this->getQueryExecutionContext(),
            $this->getResultConfiguration()
        );
    }

    protected function getQueryExecutionContext(): QueryExecutionContext
    {
        return new QueryExecutionContext(
            $this->config['catalog'] ?? null,
            $this->config['database'] ?? null
        );
    }

    protected function getResultConfiguration(): ?ResultConfiguration
    {
        if ($this->config['s3_output_bucket'] ?? null) {
            return new ResultConfiguration(
                aclConfiguration: null,
                encryptionConfiguration: null,
                expectedBucketOwner: null,
                outputLocation: $this->config['s3_output_bucket']
            );
        }

        return null;
    }
}
