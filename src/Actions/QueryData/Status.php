<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\QueryData;

use Carbon\Carbon;
use Smorken\Athena\Actions\Data\BaseDataWithResult;
use Smorken\Athena\Constants\QueryExecutionState;

final class Status extends BaseDataWithResult
{
    public function __construct(
        public ?AthenaError $athenaError,
        public QueryExecutionState $state,
        public ?string $stateChangeReason,
        public ?Carbon $completionDateTime,
        public Carbon $submissionDateTime
    ) {}

    public static function fromArray(array $raw): static
    {
        return new self(
            athenaError: isset($raw['AthenaError']) ? AthenaError::fromArray($raw['AthenaError']) : null,
            state: QueryExecutionState::from($raw['State']),
            stateChangeReason: $raw['StateChangeReason'] ?? null,
            completionDateTime: isset($raw['CompletionDateTime']) ? Carbon::parse($raw['CompletionDateTime']) : null,
            submissionDateTime: Carbon::parse($raw['SubmissionDateTime'])
        );
    }
}
