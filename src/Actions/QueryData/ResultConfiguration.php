<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\QueryData;

use Smorken\Athena\Actions\Data\BaseDataWithResult;

final class ResultConfiguration extends BaseDataWithResult
{
    public function __construct(
        public ?AclConfiguration $aclConfiguration,
        public ?EncryptionConfiguration $encryptionConfiguration,
        public ?string $expectedBucketOwner = null,
        public ?string $outputLocation = null
    ) {}

    public static function fromArray(array $raw): static
    {
        $aclConfiguration = $raw['AclConfiguration'] ?? null;
        $encryptionConfiguration = $raw['EncryptionConfiguration'] ?? null;

        return new self(
            $aclConfiguration ? AclConfiguration::fromArray($aclConfiguration) : null,
            $encryptionConfiguration ? EncryptionConfiguration::fromArray($encryptionConfiguration) : null,
            $raw['ExpectedBucketOwner'] ?? null,
            $raw['OutputLocation'] ?? null
        );
    }
}
