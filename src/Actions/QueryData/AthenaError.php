<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\QueryData;

use Smorken\Athena\Actions\Data\BaseDataWithResult;

final class AthenaError extends BaseDataWithResult
{
    public function __construct(
        public int $errorCategory,
        public string $errorMessage,
        public int $errorType,
        public bool $retryable,
    ) {}

    public static function fromArray(array $raw): static
    {
        return new self(
            errorCategory: $raw['ErrorCategory'],
            errorMessage: $raw['ErrorMessage'],
            errorType: $raw['ErrorType'],
            retryable: $raw['Retryable']
        );
    }
}
