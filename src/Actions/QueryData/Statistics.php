<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\QueryData;

use Smorken\Athena\Actions\Data\BaseDataWithResult;

final class Statistics extends BaseDataWithResult
{
    public function __construct(
        public ?string $dataManifestLocation,
        public ?int $dataScannedInBytes,
        public ?int $engineExecutionTimeInMillis,
        public ?int $queryPlanningTimeInMillis,
        public ?int $queryQueueTimeInMillis,
        public ?ResultReuseInformation $resultReuseInformation,
        public ?int $serviceProcessingTimeInMillis,
        public ?int $totalExecutionTimeInMillis
    ) {}

    public static function fromArray(array $raw): static
    {
        return new self(
            dataManifestLocation: $raw['DataManifestLocation'] ?? null,
            dataScannedInBytes: $raw['DataScannedInBytes'] ?? null,
            engineExecutionTimeInMillis: $raw['EngineExecutionTimeInMillis'] ?? null,
            queryPlanningTimeInMillis: $raw['QueryPlanningTimeInMillis'] ?? null,
            queryQueueTimeInMillis: $raw['QueryQueueTimeInMillis'] ?? null,
            resultReuseInformation: isset($raw['ResultReuseInformation']) ? ResultReuseInformation::fromArray($raw['ResultReuseInformation']) : null,
            serviceProcessingTimeInMillis: $raw['ServiceProcessingTimeInMillis'] ?? null,
            totalExecutionTimeInMillis: $raw['TotalExecutionTimeInMillis'] ?? null
        );
    }
}
