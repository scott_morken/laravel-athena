<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\QueryData;

use Smorken\Athena\Actions\Data\BaseDataWithResult;

final class EngineVersion extends BaseDataWithResult
{
    public function __construct(
        public string $effectiveEngineVersion,
        public string $selectedEngineVersion,
    ) {}

    public static function fromArray(array $raw): static
    {
        return new self(
            $raw['EffectiveEngineVersion'],
            $raw['SelectedEngineVersion']
        );
    }
}
