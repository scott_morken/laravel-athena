<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\QueryData;

use Smorken\Athena\Actions\Data\BaseDataWithResult;

final class ResultReuseByAgeConfiguration extends BaseDataWithResult
{
    public function __construct(
        public bool $enabled,
        public ?int $maxAgeInMinutes = null
    ) {}

    public static function fromArray(array $raw): static
    {
        return new self(
            enabled: $raw['Enabled'] ?? false,
            maxAgeInMinutes: $raw['MaxAgeInMinutes'] ?? null
        );
    }
}
