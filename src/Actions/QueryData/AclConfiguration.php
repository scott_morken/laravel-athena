<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\QueryData;

use Smorken\Athena\Actions\Data\BaseDataWithResult;
use Smorken\Athena\Constants\S3AclOption;

final class AclConfiguration extends BaseDataWithResult
{
    public function __construct(
        public S3AclOption $s3AclOption = S3AclOption::BUCKET_OWNER_FULL_CONTROL
    ) {}

    public static function fromArray(array $raw): static
    {
        return new self(
            s3AclOption: $raw['S3AclOption'] ?? S3AclOption::BUCKET_OWNER_FULL_CONTROL
        );
    }
}
