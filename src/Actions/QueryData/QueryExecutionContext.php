<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\QueryData;

use Smorken\Athena\Actions\Data\BaseDataWithResult;

final class QueryExecutionContext extends BaseDataWithResult
{
    public function __construct(
        public ?string $catalog,
        public ?string $database,
    ) {}

    public static function fromArray(array $raw): static
    {
        return new self(
            catalog: $raw['Catalog'] ?? null,
            database: $raw['Database'] ?? null
        );
    }
}
