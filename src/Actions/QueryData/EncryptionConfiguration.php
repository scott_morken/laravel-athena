<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\QueryData;

use Smorken\Athena\Actions\Data\BaseDataWithResult;
use Smorken\Athena\Constants\EncryptionOption;

final class EncryptionConfiguration extends BaseDataWithResult
{
    public function __construct(
        public EncryptionOption $encryptionOption,
        public ?string $kmsKey = null
    ) {}

    public static function fromArray(array $raw): static
    {
        return new self(
            encryptionOption: EncryptionOption::from($raw['EncryptionOption']),
            kmsKey: $raw['KmsKey'] ?? null
        );
    }
}
