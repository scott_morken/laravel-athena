<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\QueryData;

use Smorken\Athena\Actions\Data\BaseDataWithResult;

final class QueryExecutionId extends BaseDataWithResult
{
    public function __construct(
        public string $queryExecutionId
    ) {}

    public static function fromArray(array $raw): static
    {
        return new self($raw['QueryExecutionId']);
    }
}
