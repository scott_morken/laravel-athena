<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\Data;

use Aws\ResultInterface;
use Illuminate\Support\Collection;
use Smorken\Athena\Contracts\Parts\Result;

abstract class BaseResultCollection extends Collection implements Result
{
    public static function fromAwsResult(ResultInterface $result): static
    {
        return static::fromArray($result->toArray());
    }
}
