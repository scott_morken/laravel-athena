<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\Data;

use Smorken\Athena\Concerns\HasPropertiesToArray;
use Smorken\Athena\Contracts\Actions\Data;

abstract class BaseData implements Data
{
    use HasPropertiesToArray;
}
