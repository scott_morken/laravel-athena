<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\Data;

use Aws\ResultInterface;
use Smorken\Athena\Contracts\Actions\HasResult;

abstract class BaseDataWithResult extends BaseData implements HasResult
{
    public static function fromAwsResult(ResultInterface $result): static
    {
        return static::fromArray($result->toArray());
    }
}
