<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions;

use Aws\Athena\AthenaClient;
use Smorken\Athena\Contracts\Actions\WithClient;

abstract class BaseAction implements WithClient
{
    public function __construct(protected AthenaClient $client) {}

    public function getClient(): AthenaClient
    {
        return $this->client;
    }
}
