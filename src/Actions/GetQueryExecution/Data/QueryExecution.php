<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\GetQueryExecution\Data;

use Smorken\Athena\Actions\Data\BaseDataWithResult;
use Smorken\Athena\Actions\QueryData\EngineVersion;
use Smorken\Athena\Actions\QueryData\QueryExecutionContext;
use Smorken\Athena\Actions\QueryData\ResultConfiguration;
use Smorken\Athena\Actions\QueryData\ResultReuseConfiguration;
use Smorken\Athena\Actions\QueryData\Statistics;
use Smorken\Athena\Actions\QueryData\Status;
use Smorken\Athena\Constants\StatementType;

final class QueryExecution extends BaseDataWithResult
{
    public function __construct(
        public string $queryExecutionId,
        public string $query,
        public array $executionParameters,
        public QueryExecutionContext $queryExecutionContext,
        public ?EngineVersion $engineVersion,
        public ?ResultConfiguration $resultConfiguration,
        public ?ResultReuseConfiguration $resultReuseConfiguration,
        public StatementType $statementType,
        public ?Statistics $statistics,
        public Status $status,
        public ?string $substatementType,
        public ?string $workGroup
    ) {}

    public static function fromArray(array $raw): static
    {
        return new self(
            queryExecutionId: $raw['QueryExecutionId'],
            query: $raw['Query'],
            executionParameters: $raw['ExecutionParameters'] ?? [],
            queryExecutionContext: QueryExecutionContext::fromArray($raw['QueryExecutionContext']),
            engineVersion: isset($raw['EngineVersion']) ? EngineVersion::fromArray($raw['EngineVersion']) : null,
            resultConfiguration: isset($raw['ResultConfiguration']) ? ResultConfiguration::fromArray($raw['ResultConfiguration']) : null,
            resultReuseConfiguration: isset($raw['ResultReuseConfiguration']) ? ResultReuseConfiguration::fromArray($raw['ResultReuseConfiguration']) : null,
            statementType: StatementType::from($raw['StatementType']),
            statistics: isset($raw['Statistics']) ? Statistics::fromArray($raw['Statistics']) : null,
            status: Status::fromArray($raw['Status']),
            substatementType: $raw['SubstatementType'] ?? null,
            workGroup: $raw['WorkGroup'] ?? null
        );
    }
}
