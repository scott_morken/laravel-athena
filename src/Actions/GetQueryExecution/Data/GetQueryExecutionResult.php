<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\GetQueryExecution\Data;

use Smorken\Athena\Actions\Data\BaseDataWithResult;
use Smorken\Athena\Actions\QueryData\Status;

final class GetQueryExecutionResult extends BaseDataWithResult
{
    public function __construct(public QueryExecution $queryExecution) {}

    public static function fromArray(array $raw): static
    {
        return new self(QueryExecution::fromArray($raw['QueryExecution']));
    }

    public function isFailed(): bool
    {
        return $this->getStatus()->state->isFailed();
    }

    public function isPending(): bool
    {
        return $this->getStatus()->state->isPending();
    }

    public function isComplete(): bool
    {
        return $this->getStatus()->state->isCompleted();
    }

    protected function getStatus(): Status
    {
        return $this->queryExecution->status;
    }
}
