<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\GetQueryExecution;

use Smorken\Athena\Actions\BaseAction;
use Smorken\Athena\Actions\GetQueryExecution\Data\GetQueryExecutionResult;
use Smorken\Athena\Actions\QueryData\QueryExecutionId;

class GetQueryExecutionAction extends BaseAction implements \Smorken\Athena\Contracts\Actions\GetQueryExecutionAction
{
    public function __invoke(QueryExecutionId $queryExecutionId): GetQueryExecutionResult
    {
        $result = $this->getClient()->getQueryExecution($queryExecutionId->toArray());

        return GetQueryExecutionResult::fromAwsResult($result);
    }
}
