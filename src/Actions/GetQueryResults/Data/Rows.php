<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\GetQueryResults\Data;

use Smorken\Athena\Actions\Data\BaseResultCollection;

final class Rows extends BaseResultCollection
{
    public static function fromArray(array $raw): static
    {
        $items = array_map(fn (array $item) => Row::fromArray($item), $raw);

        return new self($items);
    }
}
