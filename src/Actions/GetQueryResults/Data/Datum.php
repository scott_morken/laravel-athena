<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\GetQueryResults\Data;

use Smorken\Athena\Actions\Data\BaseDataWithResult;

final class Datum extends BaseDataWithResult
{
    public function __construct(public string $varCharValue) {}

    public static function fromArray(array $raw): static
    {
        return new self($raw['VarCharValue'] ?? '');
    }
}
