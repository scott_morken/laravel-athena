<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\GetQueryResults\Data;

use Smorken\Athena\Actions\Data\BaseData;

final class GetQueryResultsParams extends BaseData
{
    public function __construct(
        public string $queryExecutionId,
        public ?string $nextToken = null,
        public ?int $maxResults = null,
    ) {}
}
