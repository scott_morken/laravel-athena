<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\GetQueryResults\Data;

use Smorken\Athena\Actions\Data\BaseDataWithResult;

final class ResultSet extends BaseDataWithResult
{
    public function __construct(
        public ResultSetMetadata $resultSetMetadata,
        public Rows $rows
    ) {}

    public static function fromArray(array $raw): static
    {
        return new self(
            ResultSetMetadata::fromArray($raw['ResultSetMetadata']),
            Rows::fromArray($raw['Rows'])
        );
    }
}
