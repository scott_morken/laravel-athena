<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\GetQueryResults\Data;

use Smorken\Athena\Actions\Data\BaseResultCollection;

final class ResultSetMetadata extends BaseResultCollection
{
    public static function fromArray(array $raw): static
    {
        $items = array_map(fn (array $item) => ColumnInfo::fromArray($item), $raw['ColumnInfo']);

        return new self($items);
    }
}
