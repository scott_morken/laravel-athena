<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\GetQueryResults\Data;

use Smorken\Athena\Actions\Data\BaseDataWithResult;

final class GetQueryResults extends BaseDataWithResult
{
    public function __construct(
        public ResultSet $resultSet,
        public ?string $nextToken,
        public ?int $updateCount,
    ) {}

    public static function fromArray(array $raw): static
    {
        return new self(
            ResultSet::fromArray($raw['ResultSet']),
            $raw['NextToken'] ?? null,
            $raw['UpdateCount'] ?? null
        );
    }

    public function hasNext(): bool
    {
        return (bool) $this->nextToken;
    }
}
