<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\GetQueryResults\Data;

use Smorken\Athena\Actions\Data\BaseDataWithResult;
use Smorken\Athena\Constants\ColumnType;
use Smorken\Athena\Constants\NullableColumn;

final class ColumnInfo extends BaseDataWithResult
{
    public function __construct(
        public string $name,
        public ColumnType $type,
        public bool $caseSensitive,
        public string $catalogName,
        public string $tableName,
        public string $label,
        public NullableColumn $nullable,
        public int $precision,
        public int $scale,

    ) {}

    public static function fromArray(array $raw): static
    {
        return new self(
            name: $raw['Name'],
            type: ColumnType::from($raw['Type']),
            caseSensitive: $raw['CaseSensitive'],
            catalogName: $raw['CatalogName'],
            tableName: $raw['TableName'],
            label: $raw['Label'],
            nullable: NullableColumn::from($raw['Nullable']),
            precision: $raw['Precision'],
            scale: $raw['Scale']
        );
    }
}
