<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\GetQueryResults;

use Smorken\Athena\Actions\GetQueryResults\Data\GetQueryResultsParams;
use Smorken\Athena\Actions\QueryData\QueryExecutionId;

class ParamsBuilder
{
    public function __construct(protected array $config) {}

    public function params(
        QueryExecutionId $queryExecutionId,
        ?string $nextToken = null,
        ?int $maxResults = null
    ): GetQueryResultsParams {
        return new GetQueryResultsParams(
            $queryExecutionId->queryExecutionId,
            $nextToken,
            $maxResults
        );
    }
}
