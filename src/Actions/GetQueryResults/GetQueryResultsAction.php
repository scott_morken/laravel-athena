<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\GetQueryResults;

use Smorken\Athena\Actions\BaseAction;
use Smorken\Athena\Actions\GetQueryResults\Data\GetQueryResults;
use Smorken\Athena\Actions\GetQueryResults\Data\GetQueryResultsParams;

class GetQueryResultsAction extends BaseAction implements \Smorken\Athena\Contracts\Actions\GetQueryResultsAction
{
    public function __invoke(GetQueryResultsParams $params): GetQueryResults
    {
        $result = $this->getClient()->getQueryResults($params->toArray());

        return GetQueryResults::fromAwsResult($result);
    }
}
