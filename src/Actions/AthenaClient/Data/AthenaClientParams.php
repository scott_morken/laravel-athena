<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\AthenaClient\Data;

use Smorken\Athena\Actions\Data\BaseData;

class AthenaClientParams extends BaseData
{
    protected bool $ucFirstChar = false;

    public function __construct(
        public Credentials $credentials,
        public string $region = 'us-west-2',
        public string $version = 'latest',
        public bool $debug = false,
    ) {}
}
