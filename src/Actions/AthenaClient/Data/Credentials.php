<?php

declare(strict_types=1);

namespace Smorken\Athena\Actions\AthenaClient\Data;

use Aws\Credentials\CredentialsInterface;
use Smorken\Athena\Actions\Data\BaseData;

class Credentials extends BaseData implements CredentialsInterface
{
    protected bool $ucFirstChar = false;

    public function __construct(
        public string $key,
        public string $secret,
    ) {}

    public function getAccessKeyId(): string
    {
        return $this->key;
    }

    public function getExpiration(): ?string
    {
        return null;
    }

    public function getSecretKey(): string
    {
        return $this->secret;
    }

    public function getSecurityToken(): ?string
    {
        return null;
    }

    public function isExpired(): bool
    {
        return true;
    }
}
