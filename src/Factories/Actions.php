<?php

declare(strict_types=1);

namespace Smorken\Athena\Factories;

use Aws\Athena\AthenaClient;
use Smorken\Athena\Contracts\Actions\Action;

class Actions implements \Smorken\Athena\Contracts\Factories\Actions
{
    protected array $instantiated = [];

    public function __construct(protected array $actions) {}

    public function get(string $contract, AthenaClient $client): Action
    {
        if (isset($this->instantiated[$contract])) {
            return $this->instantiated[$contract];
        }
        $this->instantiated[$contract] = $this->instantiate($contract, $client);

        return $this->instantiated[$contract];
    }

    protected function instantiate(string $contract, AthenaClient $client): Action
    {
        $class = $this->actions[$contract];

        return new $class($client);
    }
}
