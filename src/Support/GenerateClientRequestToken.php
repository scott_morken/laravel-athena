<?php

declare(strict_types=1);

namespace Smorken\Athena\Support;

use Illuminate\Support\Str;

class GenerateClientRequestToken
{
    public static function generate(): string
    {
        return Str::ulid()->toBase32();
    }
}
