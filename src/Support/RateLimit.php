<?php

declare(strict_types=1);

namespace Smorken\Athena\Support;

class RateLimit
{
    protected int $count = 0;

    protected ?int $incremental = null;

    protected int $microseconds;

    public function __construct(int $milliseconds = 500, protected int $maxCount = 5)
    {
        $this->microseconds = $milliseconds * 1000;
    }

    public function reset(): void
    {
        $this->incremental = null;
        $this->count = 0;
    }

    public function sleep(bool $incremental = false): void
    {
        if ($incremental) {
            $this->incremental();
        } else {
            $this->microSleep($this->microseconds);
        }
    }

    protected function incremental(): void
    {
        if ($this->incremental === null) {
            $this->incrementalIsNew();
        } else {
            $this->incrementalContinued();
        }
        $this->microSleep($this->incremental);
    }

    protected function incrementalContinued(): void
    {
        if ($this->count < $this->maxCount) {
            $this->count++;
        }
        $this->incremental += ($this->microseconds * $this->count);
    }

    protected function microSleep(int $microseconds): void
    {
        usleep($microseconds);
    }

    protected function incrementalIsNew(): void
    {
        $this->incremental = $this->microseconds;
    }
}
