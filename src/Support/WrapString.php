<?php

declare(strict_types=1);

namespace Smorken\Athena\Support;

class WrapString
{
    public static function wrap(mixed $value): string
    {
        if (! is_string($value)) {
            $value = (string) $value;
        }
        if (! str_starts_with($value, "'")) {
            $value = "'".$value;
        }
        if (! str_ends_with($value, "'")) {
            $value .= "'";
        }

        return $value;
    }
}
