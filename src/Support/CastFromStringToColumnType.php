<?php

declare(strict_types=1);

namespace Smorken\Athena\Support;

use Smorken\Athena\Constants\ColumnType;
use Smorken\Athena\Contracts\Casts;

class CastFromStringToColumnType implements Casts
{
    /**
     * @param  array<\Smorken\Athena\Contracts\Parts\Cast>  $casts
     */
    public function __construct(protected array $casts) {}

    public function cast(string $value, ColumnType $type): mixed
    {
        foreach ($this->casts as $cast) {
            if ($cast->handles($type)) {
                return $cast->cast($value);
            }
        }

        return $value;
    }
}
