<?php

declare(strict_types=1);

namespace Smorken\Athena\Support;

use Aws\Athena\Exception\AthenaException;

class ParseAthenaException
{
    public function __construct(protected AthenaException $exception) {}

    public function command(): string
    {
        return $this->exception->getCommand()->getName();
    }

    public function statusCode(): ?int
    {
        return $this->exception->getStatusCode();
    }

    public function errorCode(): string
    {
        return $this->exception->getAwsErrorCode();
    }

    public function detail(): array
    {
        return [];
    }

    public function message(): string
    {
        return $this->exception->getAwsErrorMessage();
    }
}
