<?php

return [
    'actions' => [
        \Smorken\Athena\Contracts\Actions\GetQueryExecutionAction::class => \Smorken\Athena\Actions\GetQueryExecution\GetQueryExecutionAction::class,
        \Smorken\Athena\Contracts\Actions\GetQueryResultsAction::class => \Smorken\Athena\Actions\GetQueryResults\GetQueryResultsAction::class,
        \Smorken\Athena\Contracts\Actions\StartQueryExecutionAction::class => \Smorken\Athena\Actions\StartQueryExecution\StartQueryExecutionAction::class,
    ],
    'database' => [
        'credentials' => [
            'key' => env('ATHENA_ACCESS_KEY', env('AWS_ACCESS_KEY_ID')),
            'secret' => env('ATHENA_SECRET', env('AWS_SECRET_ACCESS_KEY')),
        ],
        'region' => env('ATHENA_REGION', env('AWS_REGION', 'us-west-2')),
        'version' => 'latest',
        'database' => env('ATHENA_DATABASE'),
        'catalog' => env('ATHENA_CATALOG'),
        'table_prefix' => env('ATHENA_TABLE_PREFIX', ''),
        's3_output_bucket' => env('ATHENA_S3_OUTPUT_BUCKET'),
        'debug' => false,
    ],
];
