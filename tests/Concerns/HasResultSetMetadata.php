<?php

declare(strict_types=1);

namespace Tests\Smorken\Athena\Concerns;

use Smorken\Athena\Actions\GetQueryResults\Data\ColumnInfo;
use Smorken\Athena\Actions\GetQueryResults\Data\ResultSetMetadata;
use Smorken\Athena\Constants\ColumnType;
use Smorken\Athena\Constants\NullableColumn;

trait HasResultSetMetadata
{
    protected function getResultSetMetadata(array $columnInfos = []): ResultSetMetadata
    {
        if (empty($columnInfos)) {
            $columnInfos = [
                new ColumnInfo('foo', ColumnType::VARCHAR, false, '', '', '', NullableColumn::UNKNOWN, 0, 0),
                new ColumnInfo('bar', ColumnType::INTEGER, false, '', '', '', NullableColumn::UNKNOWN, 0, 0),
            ];
        }

        return new ResultSetMetadata($columnInfos);
    }
}
