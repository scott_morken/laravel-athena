<?php

namespace Tests\Smorken\Athena\Concerns;

use Illuminate\Database\ConnectionInterface;

trait WithMockConnection
{
    protected function getMockConnection(
        string $connectionClass,
        array $config = [],
        array $methods = []
    ): ConnectionInterface {
        $connection = $this->getMockBuilder($connectionClass)
            ->onlyMethods($methods)
            ->setConstructorArgs([$config])
            ->getMock();
        $connection->enableQueryLog();

        return $connection;
    }
}
