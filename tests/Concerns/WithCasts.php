<?php

declare(strict_types=1);

namespace Tests\Smorken\Athena\Concerns;

use Smorken\Athena\Contracts\Casts;
use Smorken\Athena\Generators\Casts\ToBoolean;
use Smorken\Athena\Generators\Casts\ToCarbon;
use Smorken\Athena\Generators\Casts\ToFloat;
use Smorken\Athena\Generators\Casts\ToInteger;
use Smorken\Athena\Support\CastFromStringToColumnType;

trait WithCasts
{
    protected function getCasts(): Casts
    {
        return new CastFromStringToColumnType([
            new ToBoolean(),
            new ToCarbon(),
            new ToFloat(),
            new ToInteger(),
        ]);
    }
}
