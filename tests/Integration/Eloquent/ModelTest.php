<?php

declare(strict_types=1);

namespace Tests\Smorken\Athena\Integration\Eloquent;

use Aws\Athena\AthenaClient;
use Aws\Result;
use Carbon\Carbon;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Athena\Database\AthenaConnection;
use Tests\Smorken\Athena\Stubs\ModelStub;
use Tests\Smorken\Athena\TestBenchTestCase;

class ModelTest extends TestBenchTestCase
{
    protected ?AthenaClient $client = null;

    #[Test]
    public function it_can_create_a_limit_and_offset_query(): void
    {
        $sut = (new ModelStub())->newQuery();
        $sut->offset(10)->limit(5);
        $this->assertEquals('select * from "model_stubs" offset 10 rows fetch next 5 rows only', $sut->toSql());
    }

    #[Test]
    public function it_can_create_a_limit_query(): void
    {
        $sut = (new ModelStub())->newQuery();
        $sut->limit(10);
        $this->assertEquals('select * from "model_stubs" limit 10', $sut->toSql());
    }

    #[Test]
    public function it_can_create_a_timestamp_query(): void
    {
        $date = Carbon::parse('2023-10-12');
        $sut = (new ModelStub())->newQuery();
        $sut->whereTimestamp('ts', '>', $date);
        $this->assertEquals('select * from "model_stubs" where "ts" > from_iso8601_timestamp(?)', $sut->toSql());
        $this->assertEquals(['2023-10-12T00:00:00+00:00'], $sut->getBindings());
    }

    #[Test]
    public function it_can_create_an_offset_query(): void
    {
        $sut = (new ModelStub())->newQuery();
        $sut->offset(10);
        $this->assertEquals('select * from "model_stubs" offset 10 rows', $sut->toSql());
    }

    #[Test]
    public function it_can_get_a_collection_of_models(): void
    {
        $sut = ModelStub::query()
            ->where('method', '=', 'GET')
            ->where('status', '=', 200)
            ->where('location', 'like', 'SFO%')
            ->limit(10);
        $r = new Result(['QueryExecutionId' => 'a1b2c3d4-5678-90ab-cdef-EXAMPLE11111']);
        $this->getClient()->shouldReceive('startQueryExecution')->once()->withArgs(function ($args) {
            $this->assertEquals('select * from "model_stubs" where "method" = ? and "status" = ? and "location" like ? limit 10',
                $args['QueryString']);
            $this->assertEquals(['GET', 200, 'SFO%'], $args['ExecutionParameters']);

            return true;
        })->andReturn($r);
        $resultsArray = json_decode(file_get_contents(__DIR__.'/../../data/getQueryExecution.json'), true);
        $r = new Result($resultsArray);
        $this->getClient()
            ->shouldReceive('getQueryExecution')
            ->once()
            ->with(['QueryExecutionId' => 'a1b2c3d4-5678-90ab-cdef-EXAMPLE11111'])
            ->andReturn($r);
        $resultsArray = json_decode(file_get_contents(__DIR__.'/../../data/getQueryResults.json'), true);
        $r = new Result($resultsArray);
        $this->getClient()
            ->shouldReceive('getQueryResults')
            ->once()
            ->with(['QueryExecutionId' => 'a1b2c3d4-5678-90ab-cdef-EXAMPLE11111'])
            ->andReturn($r);
        $collection = $sut->get();
        $this->assertCount(10, $collection);
        $this->assertEquals([
            [
                'date' => $collection[0]->date,
                'location' => 'SFO4',
                'browser' => 'Safari',
                'uri' => '/test-image-2.jpeg',
                'status' => 200,
            ],
            [
                'date' => $collection[1]->date,
                'location' => 'SFO4',
                'browser' => 'Opera',
                'uri' => '/test-image-2.jpeg',
                'status' => 200,
            ],
            [
                'date' => $collection[2]->date,
                'location' => 'SFO4',
                'browser' => 'Firefox',
                'uri' => '/test-image-3.jpeg',
                'status' => 200,
            ],
            [
                'date' => $collection[3]->date,
                'location' => 'SFO4',
                'browser' => 'Lynx',
                'uri' => '/test-image-3.jpeg',
                'status' => 200,
            ],
            [
                'date' => $collection[4]->date,
                'location' => 'SFO4',
                'browser' => 'IE',
                'uri' => '/test-image-2.jpeg',
                'status' => 200,
            ],
            [
                'date' => $collection[5]->date,
                'location' => 'SFO4',
                'browser' => 'Opera',
                'uri' => '/test-image-1.jpeg',
                'status' => 200,
            ],
            [
                'date' => $collection[6]->date,
                'location' => 'SFO4',
                'browser' => 'Chrome',
                'uri' => '/test-image-3.jpeg',
                'status' => 200,
            ],
            [
                'date' => $collection[7]->date,
                'location' => 'SFO4',
                'browser' => 'Firefox',
                'uri' => '/test-image-2.jpeg',
                'status' => 200,
            ],
            [
                'date' => $collection[8]->date,
                'location' => 'SFO4',
                'browser' => 'Chrome',
                'uri' => '/test-image-3.jpeg',
                'status' => 200,
            ],
            [
                'date' => $collection[9]->date,
                'location' => 'SFO4',
                'browser' => 'IE',
                'uri' => '/test-image-2.jpeg',
                'status' => 200,
            ],
        ], $collection->toArray());
    }

    #[Test]
    public function it_can_use_a_bound_parameter(): void
    {
        $sut = (new ModelStub())->newQuery();
        $sut->where('foo', '>', '100');
        $this->assertEquals('select * from "model_stubs" where "foo" > ?', $sut->toSql());
        $this->assertEquals('100', $sut->getBindings()[0]);
    }

    protected function getClient(): AthenaClient
    {
        if (! $this->client) {
            $this->client = m::mock(AthenaClient::class);
        }

        return $this->client;
    }

    protected function setUp(): void
    {
        parent::setUp();
        AthenaConnection::addAthenaClient($this->getClient(), 'athena');
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
