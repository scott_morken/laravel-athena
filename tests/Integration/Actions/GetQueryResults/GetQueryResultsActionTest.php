<?php

declare(strict_types=1);

namespace Tests\Smorken\Athena\Integration\Actions\GetQueryResults;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Athena\Actions\GetQueryResults\Data\GetQueryResults;

class GetQueryResultsActionTest extends TestCase
{
    #[Test]
    public function it_creates_results_from_array(): void
    {
        $resultsArray = json_decode(file_get_contents(__DIR__.'/../../../data/getQueryResults.json'), true);
        $sut = GetQueryResults::fromArray($resultsArray);
        $this->assertEquals(11, count($sut->resultSet->rows));
    }
}
