<?php

declare(strict_types=1);

namespace Tests\Smorken\Athena\Integration\Actions\GetQueryExecution;

use Aws\Athena\AthenaClient;
use Aws\Result;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Athena\Actions\GetQueryExecution\Data\GetQueryExecutionResult;
use Smorken\Athena\Actions\GetQueryExecution\GetQueryExecutionAction;
use Smorken\Athena\Actions\QueryData\QueryExecutionId;
use Smorken\Athena\Constants\QueryExecutionState;
use Smorken\Athena\Constants\StatementType;

class GetQueryExecutionActionTest extends TestCase
{
    #[Test]
    public function it_checks_the_status_of_a_running_query(): void
    {
        $client = m::mock(AthenaClient::class);
        $sut = new GetQueryExecutionAction($client);
        $result = new Result(json_decode(file_get_contents(__DIR__.'/../../../data/getQueryExecution.json'), true));
        $client->expects()->getQueryExecution(['QueryExecutionId' => '123'])->andReturn($result);
        $r = $sut(new QueryExecutionId('123'));
        $this->assertTrue($r->isComplete());
    }

    #[Test]
    public function it_creates_results_from_array(): void
    {
        $resultsArray = json_decode(file_get_contents(__DIR__.'/../../../data/getQueryExecution.json'), true);
        $sut = GetQueryExecutionResult::fromArray($resultsArray);
        $this->assertEquals([
            'QueryExecution' => [
                'ResultConfiguration' => [
                    'OutputLocation' => 's3://awsdoc-example-bucket/a1b2c3d4-5678-90ab-cdef-EXAMPLE11111.csv',
                ],
                'QueryExecutionContext' => [
                    'Catalog' => 'awsdatacatalog',
                    'Database' => 'mydatabase',
                ],
                'Status' => [
                    'State' => QueryExecutionState::SUCCEEDED,
                    'CompletionDateTime' => $sut->queryExecution->status->completionDateTime,
                    'SubmissionDateTime' => $sut->queryExecution->status->submissionDateTime,
                ],
                'Statistics' => [
                    'DataScannedInBytes' => 203089,
                    'EngineExecutionTimeInMillis' => 3600,
                    'QueryPlanningTimeInMillis' => 1175,
                    'QueryQueueTimeInMillis' => 267,
                    'TotalExecutionTimeInMillis' => 3821,
                ],
                'WorkGroup' => 'AthenaAdmin',
                'QueryExecutionId' => 'a1b2c3d4-5678-90ab-cdef-EXAMPLE11111',
                'Query' => "select date, location, browser, uri, status from cloudfront_logs where method = 'GET' and status = 200 and location like 'SFO%' limit 10",
                'StatementType' => StatementType::DML,
            ],
        ], $sut->toArray());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
