<?php

namespace Tests\Smorken\Athena;

use Orchestra\Testbench\TestCase;
use Smorken\Athena\ServiceProvider;

class TestBenchTestCase extends TestCase
{
    protected function getPackageProviders($app): array
    {
        return [
            ServiceProvider::class,
        ];
    }
}
