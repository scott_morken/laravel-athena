<?php

declare(strict_types=1);

namespace Tests\Smorken\Athena\Unit\Support;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Athena\Support\WrapString;

class WrapStringTest extends TestCase
{
    #[Test]
    public function it_should_wrap_a_string(): void
    {
        $this->assertEquals("'foo'", WrapString::wrap('foo'));
    }

    #[Test]
    public function it_should_wrap_an_int(): void
    {
        $this->assertEquals("'1'", WrapString::wrap(1));
    }
}
