<?php

declare(strict_types=1);

namespace Tests\Smorken\Athena\Unit\Actions\StartQueryExecution\Data;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Athena\Actions\QueryData\QueryExecutionContext;
use Smorken\Athena\Actions\QueryData\ResultConfiguration;
use Smorken\Athena\Actions\QueryData\ResultReuseByAgeConfiguration;
use Smorken\Athena\Actions\QueryData\ResultReuseConfiguration;
use Smorken\Athena\Actions\StartQueryExecution\Data\StartQueryExecutionParams;

class StartQueryExecutionParamsTest extends TestCase
{
    #[Test]
    public function it_creates_a_param_from_a_query_string(): void
    {
        $sut = new StartQueryExecutionParams('SELECT * FROM TEST');
        $this->assertEquals([
            'QueryString' => 'SELECT * FROM TEST',
        ], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_param_from_a_query_string_and_execution_parameters(): void
    {
        $sut = new StartQueryExecutionParams('SELECT * FROM TEST WHERE something = ?', ['foo']);
        $this->assertEquals([
            'QueryString' => 'SELECT * FROM TEST WHERE something = ?',
            'ExecutionParameters' => ['foo'],
        ], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_param_with_client_request_token(): void
    {
        $sut = new StartQueryExecutionParams(
            queryString: 'SELECT * FROM TEST',
            clientRequestToken: 'foo'
        );
        $this->assertEquals([
            'QueryString' => 'SELECT * FROM TEST',
            'ClientRequestToken' => 'foo',
        ], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_param_with_query_execution_context(): void
    {
        $sut = new StartQueryExecutionParams(
            queryString: 'SELECT * FROM TEST',
            queryExecutionContext: new QueryExecutionContext(null, 'database')
        );
        $this->assertEquals([
            'QueryString' => 'SELECT * FROM TEST',
            'QueryExecutionContext' => [
                'Database' => 'database',
            ],
        ], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_param_with_result_configuration(): void
    {
        $sut = new StartQueryExecutionParams(
            queryString: 'SELECT * FROM TEST',
            resultConfiguration: new ResultConfiguration(null, null, null, 's3://somewhere')
        );
        $this->assertEquals([
            'QueryString' => 'SELECT * FROM TEST',
            'ResultConfiguration' => [
                'OutputLocation' => 's3://somewhere',
            ],
        ], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_param_with_result_reuse_configuration(): void
    {
        $sut = new StartQueryExecutionParams(
            queryString: 'SELECT * FROM TEST',
            resultReuseConfiguration: new ResultReuseConfiguration(new ResultReuseByAgeConfiguration(true, 5))
        );
        $this->assertEquals([
            'QueryString' => 'SELECT * FROM TEST',
            'ResultReuseConfiguration' => [
                'ResultReuseByAgeConfiguration' => [
                    'Enabled' => true,
                    'MaxAgeInMinutes' => 5,
                ],
            ],
        ], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_param_with_workgroup(): void
    {
        $sut = new StartQueryExecutionParams(
            queryString: 'SELECT * FROM TEST',
            workGroup: 'foo'
        );
        $this->assertEquals([
            'QueryString' => 'SELECT * FROM TEST',
            'WorkGroup' => 'foo',
        ], $sut->toArray());
    }
}
