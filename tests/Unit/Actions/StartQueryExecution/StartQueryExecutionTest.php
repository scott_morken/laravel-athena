<?php

declare(strict_types=1);

namespace Tests\Smorken\Athena\Unit\Actions\StartQueryExecution;

use Aws\Athena\AthenaClient;
use Aws\Result;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Athena\Actions\StartQueryExecution\Data\StartQueryExecutionParams;
use Smorken\Athena\Actions\StartQueryExecution\StartQueryExecutionAction;

class StartQueryExecutionTest extends TestCase
{
    #[Test]
    public function it_returns_a_query_execution_id(): void
    {
        $params = new StartQueryExecutionParams('SELECT * FROM TEST');
        $client = m::mock(AthenaClient::class);
        $action = new StartQueryExecutionAction($client);
        $client->expects()->startQueryExecution(['QueryString' => 'SELECT * FROM TEST'])
            ->andReturn(new Result(['QueryExecutionId' => '123']));
        $this->assertEquals('123', $action($params)->queryExecutionId);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
