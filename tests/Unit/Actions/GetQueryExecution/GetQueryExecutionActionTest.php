<?php

declare(strict_types=1);

namespace Tests\Smorken\Athena\Unit\Actions\GetQueryExecution;

use Aws\Athena\AthenaClient;
use Aws\Result;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Athena\Actions\GetQueryExecution\GetQueryExecutionAction;
use Smorken\Athena\Actions\QueryData\QueryExecutionId;

class GetQueryExecutionActionTest extends TestCase
{
    #[Test]
    public function it_checks_the_status_of_a_running_query(): void
    {
        $client = m::mock(AthenaClient::class);
        $sut = new GetQueryExecutionAction($client);
        $result = new Result(json_decode(file_get_contents(__DIR__.'/../../../data/getQueryExecution.json'), true));
        $client->expects()->getQueryExecution(['QueryExecutionId' => '123'])->andReturn($result);
        $r = $sut(new QueryExecutionId('123'));
        $this->assertTrue($r->isComplete());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
