<?php

declare(strict_types=1);

namespace Tests\Smorken\Athena\Unit\Generators;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Athena\Constants\ColumnType;
use Smorken\Athena\Constants\NullableColumn;
use Smorken\Athena\Generators\Attribute;
use Tests\Smorken\Athena\Concerns\WithCasts;

class AttributeTest extends TestCase
{
    use WithCasts;

    #[Test]
    public function it_creates_a_boolean_attribute_from_empty_bool_when_not_nullable(): void
    {
        $sut = new Attribute('test', '', ColumnType::BOOLEAN, NullableColumn::NOT_NULL);
        $this->assertEquals(['test' => false], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_boolean_attribute_from_string_false(): void
    {
        $sut = new Attribute('test', 'false', ColumnType::BOOLEAN, NullableColumn::NULLABLE);
        $this->assertEquals(['test' => false], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_boolean_attribute_from_string_true(): void
    {
        $sut = new Attribute('test', 'true', ColumnType::BOOLEAN, NullableColumn::NULLABLE);
        $this->assertEquals(['test' => true], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_carbon_attribute_from_string(): void
    {
        $sut = new Attribute('test', '2023-09-01 00:00:00', ColumnType::DATE, NullableColumn::NULLABLE);
        $this->assertEquals(['test' => Carbon::parse('2023-09-01 00:00:00')], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_float_attribute_from_empty_decimal_when_not_nullable(): void
    {
        $sut = new Attribute('test', '', ColumnType::DECIMAL, NullableColumn::NOT_NULL);
        $this->assertEquals(['test' => 0.0], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_float_attribute_from_string(): void
    {
        $sut = new Attribute('test', '1.00', ColumnType::DECIMAL, NullableColumn::NULLABLE);
        $this->assertEquals(['test' => 1.00], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_null_attribute(): void
    {
        $sut = new Attribute('test', '', ColumnType::VARCHAR, NullableColumn::NULLABLE);
        $this->assertEquals(['test' => null], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_null_attribute_from_empty_bool(): void
    {
        $sut = new Attribute('test', '', ColumnType::BOOLEAN, NullableColumn::NULLABLE);
        $this->assertEquals(['test' => null], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_null_attribute_from_empty_date(): void
    {
        $sut = new Attribute('test', '', ColumnType::DATE, NullableColumn::NULLABLE);
        $this->assertEquals(['test' => null], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_null_attribute_from_empty_decimal(): void
    {
        $sut = new Attribute('test', '', ColumnType::DECIMAL, NullableColumn::NULLABLE);
        $this->assertEquals(['test' => null], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_string_attribute(): void
    {
        $sut = new Attribute('test', 'foo', ColumnType::VARCHAR, NullableColumn::NULLABLE);
        $this->assertEquals(['test' => 'foo'], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_string_attribute_from_empty_string_when_not_nullable(): void
    {
        $sut = new Attribute('test', '', ColumnType::VARCHAR, NullableColumn::NOT_NULL);
        $this->assertEquals(['test' => ''], $sut->toArray());
    }

    #[Test]
    public function it_creates_an_integer_attribute_from_string(): void
    {
        $sut = new Attribute('test', '1', ColumnType::INTEGER, NullableColumn::NULLABLE);
        $this->assertEquals(['test' => 1], $sut->toArray());
    }

    protected function setUp(): void
    {
        parent::setUp();
        Attribute::setCastsResolver(fn () => $this->getCasts());
    }
}
