<?php

declare(strict_types=1);

namespace Tests\Smorken\Athena\Unit\Generators;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Athena\Actions\GetQueryResults\Data\Datum;
use Smorken\Athena\Actions\GetQueryResults\Data\Row;
use Smorken\Athena\Constants\ColumnType;
use Smorken\Athena\Constants\NullableColumn;
use Smorken\Athena\Generators\Attribute;
use Smorken\Athena\Generators\Attributes;
use Tests\Smorken\Athena\Concerns\HasResultSetMetadata;
use Tests\Smorken\Athena\Concerns\WithCasts;

class AttributesTest extends TestCase
{
    use HasResultSetMetadata, WithCasts;

    #[Test]
    public function it_adds_attribute_from_datum(): void
    {
        $sut = new Attributes($this->getResultSetMetadata());
        $sut->fromDatum(new Datum('foo value'), 0);
        $this->assertEquals([
            'foo' => 'foo value',
        ], $sut->toArray());
    }

    #[Test]
    public function it_adds_attributes_via_chaining(): void
    {
        $sut = new Attributes($this->getResultSetMetadata());
        $sut->add(new Attribute('foo', 'foo value', ColumnType::VARCHAR, NullableColumn::NULLABLE))
            ->add(new Attribute('bar', '200', ColumnType::INTEGER, NullableColumn::NULLABLE));
        $this->assertEquals([
            'foo' => 'foo value',
            'bar' => 200,
        ], $sut->toArray());
    }

    #[Test]
    public function it_creates_new_instance_from_row(): void
    {
        $sut = new Attributes($this->getResultSetMetadata());
        $row = new Row([new Datum('foo value'), new Datum('200')]);
        $newSut = $sut->newFromRow($row);
        $this->assertNotSame($sut, $newSut);
        $this->assertEquals([
            'foo' => 'foo value',
            'bar' => 200,
        ], $newSut->toArray());
    }

    #[Test]
    public function it_creates_new_instance_from_row_but_does_not_add_attributes_for_metadata_row(): void
    {
        $sut = new Attributes($this->getResultSetMetadata());
        $row = new Row([new Datum('foo')]);
        $newSut = $sut->newFromRow($row);
        $this->assertNotSame($sut, $newSut);
        $this->assertEquals([], $newSut->toArray());
    }

    #[Test]
    public function it_returns_true_when_row_is_metadata(): void
    {
        $sut = new Attributes($this->getResultSetMetadata());
        $this->assertTrue($sut->isRowMetadata(new Row([new Datum('foo')])));
    }

    protected function setUp(): void
    {
        parent::setUp();
        Attribute::setCastsResolver(fn () => $this->getCasts());
    }
}
