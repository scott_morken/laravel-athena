<?php

declare(strict_types=1);

namespace Tests\Smorken\Athena\Unit\Generators;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Athena\Actions\GetQueryResults\Data\Datum;
use Smorken\Athena\Actions\GetQueryResults\Data\Row;
use Smorken\Athena\Actions\GetQueryResults\Data\Rows;
use Smorken\Athena\Contracts\Generators\Attributes;
use Smorken\Athena\Generators\Attribute;
use Smorken\Athena\Generators\AttributesGenerator;
use Tests\Smorken\Athena\Concerns\HasResultSetMetadata;
use Tests\Smorken\Athena\Concerns\WithCasts;

class AttributesGeneratorTest extends TestCase
{
    use HasResultSetMetadata, WithCasts;

    #[Test]
    public function it_can_generate_an_array(): void
    {
        $rows = new Rows([
            new Row([
                new Datum('foo'),
                new Datum('bar'),
            ]),
            new Row([
                new Datum('foo value 1'),
                new Datum('100'),
            ]),
            new Row([
                new Datum('foo value 2'),
                new Datum('200'),
            ]),
            new Row([
                new Datum('foo value 3'),
                new Datum('300'),
            ]),
        ]);
        $sut = new AttributesGenerator($this->getResultSetMetadata());
        $this->assertEquals([
            [
                'foo' => 'foo value 1',
                'bar' => 100,
            ],
            [
                'foo' => 'foo value 2',
                'bar' => 200,
            ],
            [
                'foo' => 'foo value 3',
                'bar' => 300,
            ],
        ], $sut->toArray($rows));
    }

    #[Test]
    public function it_creates_a_generator_for_rows(): void
    {
        $rows = new Rows([
            new Row([
                new Datum('foo value'),
                new Datum('200'),
            ]),
        ]);
        $sut = new AttributesGenerator($this->getResultSetMetadata());
        foreach ($sut($rows) as $attributes) {
            $this->assertInstanceOf(Attributes::class, $attributes);
        }
    }

    protected function setUp(): void
    {
        parent::setUp();
        Attribute::setCastsResolver(fn () => $this->getCasts());
    }
}
