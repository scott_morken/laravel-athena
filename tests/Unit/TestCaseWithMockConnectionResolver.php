<?php

namespace Tests\Smorken\Athena\Unit;

use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Eloquent\Model;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Athena\Database\AthenaConnection;
use Tests\Smorken\Athena\Concerns\WithMockConnection;

class TestCaseWithMockConnectionResolver extends TestCase
{
    use WithMockConnection;

    protected function setUp(): void
    {
        parent::setUp();
        $cr = new ConnectionResolver(['athena' => $this->getMockConnection(AthenaConnection::class)]);
        $cr->setDefaultConnection('athena');
        Model::setConnectionResolver($cr);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
