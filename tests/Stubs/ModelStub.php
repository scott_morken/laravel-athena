<?php

declare(strict_types=1);

namespace Tests\Smorken\Athena\Stubs;

use Smorken\Athena\Database\Eloquent;

class ModelStub extends Eloquent
{
    protected $fillable = ['date', 'location', 'browser', 'uri', 'status'];
}
